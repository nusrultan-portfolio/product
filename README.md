# Product Service

### Run the service:
```
./run.sh
```
 or
```
go run rpc/product.go -f rpc/etc/product.yaml
```
or
```
make run
```


#### Before

- migration:
```
migrate -path model/migration -database "($DB_URL)" -verbose up
```
_eg:_ 

```
migrate -path model/migration -database "postgresql://$user:$password@localhost:5432/$database?sslmode" -verbose up</sub>
```
or
```
make migrateup
```

- run etcd, port ":2379"

#### Stop product_service:3001
```
./kill3001.sh
```

#### Steps:
- run docker DB container (pq,elastic,kibana):
    - docker compose up
- create DB:
    - make createdb
- migration:
    - make migrateup
- run service:
    - make run