package logic

import (
	"context"
	"database/sql"
	"fmt"

	"ecom_kg/product/rpc/internal/svc"
	"ecom_kg/product/rpc/product"

	"github.com/zeromicro/go-zero/core/logx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type DeleteColorLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewDeleteColorLogic(ctx context.Context, svcCtx *svc.ServiceContext) *DeleteColorLogic {
	return &DeleteColorLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *DeleteColorLogic) DeleteColor(in *product.DeleteColorsReq) (*product.DeleteColorsResp, error) {
	id, err := l.svcCtx.Store.DeleteColor(l.ctx, in.Id)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, status.Errorf(codes.NotFound, "id (%v) does not exist", in.Id)
		}
		return nil, status.Errorf(codes.DeadlineExceeded, "%v", err)
	}

	return &product.DeleteColorsResp{
		Message: fmt.Sprintf("id (%v) deleted successfully", id),
	}, nil
}
