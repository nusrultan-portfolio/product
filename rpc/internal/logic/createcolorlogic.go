package logic

import (
	"context"
	"database/sql"

	model "ecom_kg/product/model/sqlc"
	"ecom_kg/product/rpc/internal/svc"
	"ecom_kg/product/rpc/product"

	"github.com/zeromicro/go-zero/core/logx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type CreateColorLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewCreateColorLogic(ctx context.Context, svcCtx *svc.ServiceContext) *CreateColorLogic {
	return &CreateColorLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *CreateColorLogic) CreateColor(in *product.CreateColorsReq) (*product.CreateColorsResp, error) {
	// todo: add your logic here and delete this line
	name := sql.NullString{}
	if in.Name != "" {
		name.String = in.Name
		name.Valid = true
	}

	family := sql.NullString{}
	if in.Family != "" {
		family.String = in.Family
		family.Valid = true
	}

	code := sql.NullString{}
	if in.Code != "" {
		code.String = in.Code
		code.Valid = true
	}

	hex := sql.NullString{}
	if in.Hex != "" {
		hex.String = in.Hex
		hex.Valid = true
	}

	arg := model.CreateColorParams{
		Abbr:   in.Abbr,
		Name:   name,
		Family: family,
		Code:   code,
		Hex:    hex,
	}

	color, err := l.svcCtx.Store.CreateColor(l.ctx, arg)
	if err != nil {
		return nil, status.Errorf(codes.DeadlineExceeded, "%v", err)
	}

	return &product.CreateColorsResp{
		Id: color.ID,
	}, nil
}
