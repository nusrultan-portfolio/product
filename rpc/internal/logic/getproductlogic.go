package logic

import (
	"context"
	"database/sql"

	"ecom_kg/product/rpc/internal/svc"
	"ecom_kg/product/rpc/product"

	"github.com/zeromicro/go-zero/core/logx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type GetProductLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetProductLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetProductLogic {
	return &GetProductLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetProductLogic) GetProduct(in *product.GetProductRequest) (*product.GetProductResponse, error) {
	// todo: add your logic here and delete this line
	getproduct, err := l.svcCtx.Store.GetProduct(l.ctx, in.Id)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, status.Errorf(codes.NotFound, "id (%v) not found", in.Id)
		}
		return nil, status.Errorf(codes.DeadlineExceeded, "%v", err)
	}

	return &product.GetProductResponse{
		Id:          getproduct.ID,
		Title:       getproduct.Title,
		Active:      getproduct.Active,
		Sku:         getproduct.Sku,
		Price:       getproduct.Price,
		Description: getproduct.Description,
		CreatedAt:   getproduct.CreatedAt.String(),
		UpdatedAt:   getproduct.UpdatedAt.String(),
	}, nil
}
