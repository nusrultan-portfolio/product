package logic

import (
	"context"

	model "ecom_kg/product/model/sqlc"
	"ecom_kg/product/rpc/internal/svc"
	"ecom_kg/product/rpc/product"

	"github.com/zeromicro/go-zero/core/logx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ListCategoriesLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewListCategoriesLogic(ctx context.Context, svcCtx *svc.ServiceContext) *ListCategoriesLogic {
	return &ListCategoriesLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *ListCategoriesLogic) ListCategories(in *product.ListCategoriesReq) (*product.ListCategoriesResp, error) {
	// todo: add your logic here and delete this line
	arg := model.ListCategoriesParams{
		Limit:  in.PageSize,
		Offset: (in.PageId - 1) * in.PageSize,
	}

	categories, err := l.svcCtx.Store.ListCategories(l.ctx, arg)
	if err != nil {
		return nil, status.Errorf(codes.DeadlineExceeded, "%v", err)
	}

	var listCategories = make([]product.Category, len(categories))

	for i := 0; i < len(categories); i++ {
		listCategories[i].Id = categories[i].ID
		listCategories[i].Name = categories[i].Name
		listCategories[i].Slug = categories[i].Slug
		listCategories[i].CreatedAt = categories[i].CreatedAt.String()
		listCategories[i].UpdatedAt = categories[i].UpdatedAt.String()
	}

	respCategories := make([]*product.Category, 0)
	for i := 0; i < len(listCategories); i++ {
		respCategories = append(respCategories, &listCategories[i])
	}

	return &product.ListCategoriesResp{
		Categories: respCategories,
	}, nil
}
