package logic

import (
	"context"
	"database/sql"

	"ecom_kg/product/rpc/internal/svc"
	"ecom_kg/product/rpc/product"

	"github.com/zeromicro/go-zero/core/logx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type GetCategoryLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetCategoryLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetCategoryLogic {
	return &GetCategoryLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetCategoryLogic) GetCategory(in *product.GetCategoryReq) (*product.GetCategoryResp, error) {
	// todo: add your logic here and delete this line
	get, err := l.svcCtx.Store.GetCategoryByID(l.ctx, in.Id)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, status.Errorf(codes.NotFound, "id (%v) not found", in.Id)
		}
		return nil, status.Errorf(codes.DeadlineExceeded, "%v", err)
	}

	return &product.GetCategoryResp{
		Id:        get.ID,
		Name:      get.Name,
		Slug:      get.Slug,
		CreatedAt: get.CreatedAt.String(),
		UpdatedAt: get.UpdatedAt.String(),
	}, nil
}
