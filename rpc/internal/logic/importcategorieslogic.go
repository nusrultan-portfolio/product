package logic

import (
	"context"
	"encoding/csv"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"strconv"

	model "ecom_kg/product/model/sqlc"
	"ecom_kg/product/rpc/internal/svc"
	"ecom_kg/product/rpc/product"

	"github.com/zeromicro/go-zero/core/logx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ImportCategoriesLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewImportCategoriesLogic(ctx context.Context, svcCtx *svc.ServiceContext) *ImportCategoriesLogic {
	return &ImportCategoriesLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *ImportCategoriesLogic) ImportCategories(in *product.ImportFormCSVReq) (*product.ImportFormCSVResp, error) {
	// todo: add your logic here and delete this line

	isLink := func(input string) bool {
		_, err := url.ParseRequestURI(input)
		return err == nil
	}

	datas := make([]model.ImportCategoriesParams, 0)
	var records [][]string

	if isLink(in.Path) {
		resp, err := http.Get(in.Path)
		if err != nil {
			return nil, status.Error(codes.NotFound, err.Error())
		}
		defer resp.Body.Close()

		reader := csv.NewReader(resp.Body)

		records, err = reader.ReadAll()
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}

	} else {

		file, err := os.Open(in.Path)
		if err != nil {
			fmt.Println(err)
		}
		reader := csv.NewReader(file)
		records, err = reader.ReadAll()
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
	}

	for i := 1; i < len(records); i++ {
		id, _ := strconv.ParseInt((records[i][0]), 10, 64)
		datas = append(datas, model.ImportCategoriesParams{ID: id, Name: records[i][1], Slug: records[i][2]})
	}

	for _, v := range datas {
		err := l.svcCtx.Store.ImportCategories(context.Background(), v)
		if err != nil {
			return nil, status.Error(codes.DeadlineExceeded, err.Error())
		}
	}

	return &product.ImportFormCSVResp{
		Status: codes.OK.String(),
	}, nil
}
