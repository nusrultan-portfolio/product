package logic

import (
	"context"
	"database/sql"
	"time"

	model "ecom_kg/product/model/sqlc"
	"ecom_kg/product/rpc/internal/svc"
	"ecom_kg/product/rpc/product"

	"github.com/zeromicro/go-zero/core/logx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type UpdateCategoryLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewUpdateCategoryLogic(ctx context.Context, svcCtx *svc.ServiceContext) *UpdateCategoryLogic {
	return &UpdateCategoryLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *UpdateCategoryLogic) UpdateCategory(in *product.UpdateCategoryReq) (*product.UpdateCategoryResp, error) {
	arg := model.UpdateCategoryParams{
		ID:        in.Id,
		Name:      in.Name,
		Slug:      in.Slug,
		UpdatedAt: time.Now(),
	}

	updated, err := l.svcCtx.Store.UpdateCategory(l.ctx, arg)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, status.Errorf(codes.NotFound, "id (%v) not found", in.Id)
		}
		return nil, status.Errorf(codes.DeadlineExceeded, "%v", err)
	}

	return &product.UpdateCategoryResp{
		Id:        updated.ID,
		Name:      updated.Name,
		Slug:      updated.Slug,
		CreatedAt: updated.CreatedAt.String(),
		UpdatedAt: updated.UpdatedAt.String(),
	}, nil
}
