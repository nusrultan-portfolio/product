package logic

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"strings"

	"ecom_kg/product/rpc/internal/svc"
	"ecom_kg/product/rpc/product"

	"github.com/zeromicro/go-zero/core/logx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type SearchProductLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

const ElasticNeworkID = "06a917ddfcb3c9bacf4dc4ab5a1350389db7796fbbe7d37c3ddbed09264e847e"

func NewSearchProductLogic(ctx context.Context, svcCtx *svc.ServiceContext) *SearchProductLogic {
	return &SearchProductLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *SearchProductLogic) SearchProduct(in *product.SearchProductRequest) (*product.SearchProductResponse, error) {
	// todo: add your logic here and delete this line

	body := fmt.Sprintf(`{
		"query": {
		  "multi_match": {
			"query": "%s",
			"type": "bool_prefix",
			"fields": [
			  "title",
			  "body"
			]
		  }
		}
	  }`, in.Words)

	res, err := l.svcCtx.ESClient.Search(l.svcCtx.ESClient.Search.WithContext(l.ctx),
		l.svcCtx.ESClient.Search.WithIndex("students"),
		l.svcCtx.ESClient.Search.WithBody(strings.NewReader(body)),
		l.svcCtx.ESClient.Search.WithPretty(),
	)
	if err != nil {
		log.Println(err)
		return nil, status.Errorf(codes.Internal, "%v", err)
	}
	defer res.Body.Close()

	var r map[string]interface{}
	if err := json.NewDecoder(res.Body).Decode(&r); err != nil {
		return nil, err
	}

	// for _, v := range r["hits"].(map[string]interface{})["hits"].([]interface{}) {
	// 	log.Printf("%s - %v\n", v.(map[string]interface{})["_id"], v.(map[string]interface{})["_source"])
	// }

	log.Println(res)

	jsonRes, err := json.Marshal(r["hits"])
	if err != nil {
		log.Println(err)
	}

	log.Println(string(jsonRes))

	return &product.SearchProductResponse{
		Result: string(jsonRes),
	}, nil
}
