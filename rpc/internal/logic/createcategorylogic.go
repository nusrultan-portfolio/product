package logic

import (
	"context"

	model "ecom_kg/product/model/sqlc"
	"ecom_kg/product/rpc/internal/svc"
	"ecom_kg/product/rpc/product"

	"github.com/zeromicro/go-zero/core/logx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type CreateCategoryLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewCreateCategoryLogic(ctx context.Context, svcCtx *svc.ServiceContext) *CreateCategoryLogic {
	return &CreateCategoryLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *CreateCategoryLogic) CreateCategory(in *product.CreateCategoryReq) (*product.CreateCategoryResp, error) {
	// todo: add your logic here and delete this line
	arg := model.CreateCategoryParams{
		Name: in.Name,
		Slug: in.Slug,
	}

	created, err := l.svcCtx.Store.CreateCategory(l.ctx, arg)
	if err != nil {
		return nil, status.Errorf(codes.DeadlineExceeded, "%v", err)
	}

	return &product.CreateCategoryResp{
		Id: created.ID,
	}, nil
}
