package logic

import (
	"context"
	"database/sql"
	"time"

	model "ecom_kg/product/model/sqlc"
	"ecom_kg/product/rpc/internal/svc"
	"ecom_kg/product/rpc/product"

	"github.com/zeromicro/go-zero/core/logx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type UpdateColorLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewUpdateColorLogic(ctx context.Context, svcCtx *svc.ServiceContext) *UpdateColorLogic {
	return &UpdateColorLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *UpdateColorLogic) UpdateColor(in *product.UpdateColorsReq) (*product.UpdateColorsResp, error) {
	// todo: add your logic here and delete this line
	name := sql.NullString{}
	if in.Name != "" {
		name.String = in.Name
		name.Valid = true
	}

	family := sql.NullString{}
	if in.Family != "" {
		family.String = in.Family
		family.Valid = true
	}

	code := sql.NullString{}
	if in.Code != "" {
		code.String = in.Code
		code.Valid = true
	}

	hex := sql.NullString{}
	if in.Hex != "" {
		hex.String = in.Hex
		hex.Valid = true
	}

	arg := model.UpdateColorParams{
		ID:     in.Id,
		Abbr:   in.Abbr,
		Name:   name,
		Family: family,
		Code:   code,
		Hex:    hex,
		UpdatedAt: sql.NullTime{
			Time:  time.Now(),
			Valid: true,
		},
	}

	color, err := l.svcCtx.Store.UpdateColor(l.ctx, arg)
	if err != nil {
		return nil, status.Errorf(codes.DeadlineExceeded, "%v", err)
	}
	return &product.UpdateColorsResp{
		Id:        color.ID,
		Abbr:      color.Abbr,
		Name:      color.Name.String,
		Family:    color.Family.String,
		Code:      color.Code.String,
		Hex:       color.Hex.String,
		CreatedAt: color.CreatedAt.Time.String(),
		UpdatedAt: color.UpdatedAt.Time.String(),
	}, nil
}
