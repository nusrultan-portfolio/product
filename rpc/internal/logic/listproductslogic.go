package logic

import (
	"context"

	model "ecom_kg/product/model/sqlc"
	"ecom_kg/product/rpc/internal/svc"
	"ecom_kg/product/rpc/product"

	"github.com/zeromicro/go-zero/core/logx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ListProductsLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewListProductsLogic(ctx context.Context, svcCtx *svc.ServiceContext) *ListProductsLogic {
	return &ListProductsLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *ListProductsLogic) ListProducts(in *product.ListProductsRequest) (*product.ListProductsResponse, error) {
	// todo: add your logic here and delete this line

	arg := model.ListProductsParams{
		Limit:  in.PageSize,
		Offset: (in.PageId - 1) * in.PageSize,
	}

	products, err := l.svcCtx.Store.ListProducts(l.ctx, arg)
	if err != nil {
		return nil, status.Errorf(codes.Aborted, "listProduct err -%v", err)
	}

	var listProducts = make([]product.Product, len(products))

	for i := 0; i < len(products); i++ {
		listProducts[i].Id = products[i].ID
		listProducts[i].Title = products[i].Title
		listProducts[i].Active = products[i].Active
		listProducts[i].Sku = products[i].Sku
		listProducts[i].Price = products[i].Price
		listProducts[i].Description = products[i].Description
		listProducts[i].CreatedAt = products[i].CreatedAt.String()
		listProducts[i].UpdatedAt = products[i].UpdatedAt.String()
	}

	respList := make([]*product.Product, 0)
	for i := 0; i < len(listProducts); i++ {
		respList = append(respList, &listProducts[i])
	}

	return &product.ListProductsResponse{
		Product: respList,
	}, nil
}
