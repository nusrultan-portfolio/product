package logic

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"
	"time"

	model "ecom_kg/product/model/sqlc"
	"ecom_kg/product/rpc/internal/common/errorx"
	"ecom_kg/product/rpc/internal/svc"
	"ecom_kg/product/rpc/product"

	"github.com/go-playground/validator/v10"
	"github.com/zeromicro/go-zero/core/logx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type UpdateProductLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewUpdateProductLogic(ctx context.Context, svcCtx *svc.ServiceContext) *UpdateProductLogic {
	return &UpdateProductLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *UpdateProductLogic) UpdateProduct(in *product.UpdateProductRequest) (*product.UpdateProductResponse, error) {
	// todo: add your logic here and delete this line
	var ValidStruct struct {
		Title       string  `json:"title" validate:"required,printascii"`
		Active      bool    `json:"active" validate:""`
		Sku         string  `json:"sku" validate:"required,printascii"`
		Price       float64 `json:"price" validate:"required,numeric"`
		Description string  `json:"description" validate:"required"`
	}

	val := ValidStruct
	val.Title = in.Title
	val.Active = in.Active
	val.Sku = in.Sku
	val.Price = in.Price
	val.Description = in.Description

	logx.Infof("****Val ValidStr: %v", val)

	validate := validator.New()
	err := validate.Struct(val)

	var dataslice []errorx.Data
	if err != nil {

		if _, ok := err.(*validator.InvalidValidationError); ok {
			fmt.Println(err)
			return nil, err
		}

		for _, err := range err.(validator.ValidationErrors) {
			data := errorx.Data{}
			switch err.Field() {
			case "Title":
				data.Field = strings.ToLower(err.Field())
				data.ErrMsg = append(data.ErrMsg, err.ActualTag())
			// case "Active":
			// 	data.Field = strings.ToLower(err.Field())
			// 	data.ErrMsg = append(data.ErrMsg, err.ActualTag())
			case "Sku":
				data.Field = strings.ToLower(err.Field())
				data.ErrMsg = append(data.ErrMsg, err.ActualTag())
			case "Price":
				data.Field = strings.ToLower(err.Field())
				data.ErrMsg = append(data.ErrMsg, err.ActualTag())
			case "Description":
				data.Field = strings.ToLower(err.Field())
				data.ErrMsg = append(data.ErrMsg, err.ActualTag())
			}

			dataslice = append(dataslice, data)
		}

		var errResp = errorx.ErrorResponse{
			Code: int64(codes.InvalidArgument),
			Msg:  codes.InvalidArgument.String(),
			Data: dataslice,
		}

		jsn, _ := json.Marshal(errResp)

		return nil, status.Errorf(codes.InvalidArgument, "%v", string(jsn))
	}

	arg := model.UpdateProductParams{
		ID:          in.Id,
		Title:       in.Title,
		Active:      in.Active,
		Sku:         in.Sku,
		Price:       in.Price,
		Description: in.Description,
		UpdatedAt:   time.Now(),
	}

	updated, err := l.svcCtx.Store.UpdateProduct(l.ctx, arg)
	if err != nil {
		return nil, status.Errorf(codes.Aborted, "%v", err)
	}

	return &product.UpdateProductResponse{
		Id:          updated.ID,
		Title:       updated.Title,
		Active:      updated.Active,
		Sku:         updated.Sku,
		Price:       updated.Price,
		Description: updated.Description,
		CreatedAt:   updated.CreatedAt.String(),
		UpdatedAt:   updated.UpdatedAt.String(),
	}, nil
}
