package logic

import (
	"context"

	"ecom_kg/product/rpc/internal/svc"
	"ecom_kg/product/rpc/product"

	"github.com/zeromicro/go-zero/core/logx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type DeleteAllCategoriesLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewDeleteAllCategoriesLogic(ctx context.Context, svcCtx *svc.ServiceContext) *DeleteAllCategoriesLogic {
	return &DeleteAllCategoriesLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *DeleteAllCategoriesLogic) DeleteAllCategories(in *product.DeleteAllReq) (*product.DeleteAllResp, error) {
	// todo: add your logic here and delete this line
	err := l.svcCtx.Store.DeleteAllCategories(l.ctx)
	if err != nil {
		return nil, status.Error(codes.DeadlineExceeded, err.Error())
	}

	return &product.DeleteAllResp{
		Status: "ok",
	}, nil
}
