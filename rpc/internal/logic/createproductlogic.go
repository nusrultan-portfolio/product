package logic

import (
	"context"
	"encoding/json"
	"fmt"
	"strconv"
	"strings"

	model "ecom_kg/product/model/sqlc"
	"ecom_kg/product/rpc/internal/common/errorx"
	"ecom_kg/product/rpc/internal/svc"
	"ecom_kg/product/rpc/product"

	"github.com/go-playground/validator/v10"
	"github.com/zeromicro/go-zero/core/logx"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type CreateProductLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewCreateProductLogic(ctx context.Context, svcCtx *svc.ServiceContext) *CreateProductLogic {
	return &CreateProductLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *CreateProductLogic) CreateProduct(in *product.CreateProductRequest) (*product.CreateProductResponse, error) {
	// todo: add your logic here and delete this line

	var ValidStruct struct {
		Title       string  `json:"title" validate:"required,printascii"`
		Active      bool    `json:"active" validate:""`
		Sku         string  `json:"sku" validate:"required,printascii"`
		Price       float64 `json:"price" validate:"required,numeric"`
		Description string  `json:"description" validate:"required"`
	}

	val := ValidStruct
	val.Title = in.Title
	val.Active = in.Active
	val.Sku = in.Sku
	val.Price = in.Price
	val.Description = in.Description

	logx.Infof("****Val ValidStr: %v", val)
	logx.Infof("****Val Active type: %T val:%[1]v, t:%[1]t", val.Active)

	tip := strconv.FormatBool(val.Active)
	logx.Infof("****Val tip type: %T val:%[1]v, t:%[1]t", tip)

	validate := validator.New()
	err := validate.Struct(val)

	var dataslice []errorx.Data
	if err != nil {

		if _, ok := err.(*validator.InvalidValidationError); ok {
			fmt.Println(err)
			return nil, err
		}

		for _, err := range err.(validator.ValidationErrors) {
			data := errorx.Data{}
			switch err.Field() {
			case "Title":
				data.Field = strings.ToLower(err.Field())
				data.ErrMsg = append(data.ErrMsg, err.ActualTag())
			// case "Active":
			// 	data.Field = strings.ToLower(err.Field())
			// 	data.ErrMsg = append(data.ErrMsg, err.ActualTag())
			case "Sku":
				data.Field = strings.ToLower(err.Field())
				data.ErrMsg = append(data.ErrMsg, err.ActualTag())
			case "Price":
				data.Field = strings.ToLower(err.Field())
				data.ErrMsg = append(data.ErrMsg, err.ActualTag())
			case "Description":
				data.Field = strings.ToLower(err.Field())
				data.ErrMsg = append(data.ErrMsg, err.ActualTag())
			}

			// if tip := strconv.FormatBool(val.Active); tip != "true" || tip != "false" {
			// 	data.Field = "active"
			// 	data.ErrMsg = append(data.ErrMsg, "boolean")
			// }

			dataslice = append(dataslice, data)
		}

		var errResp = errorx.ErrorResponse{
			Code: int64(codes.InvalidArgument),
			Msg:  codes.InvalidArgument.String(),
			Data: dataslice,
		}

		jsn, _ := json.Marshal(errResp)

		return nil, status.Errorf(codes.InvalidArgument, "%v", string(jsn))
	}

	arg := model.CreateProductParams{
		Title:       in.Title,
		Active:      in.Active,
		Sku:         in.Sku,
		Price:       in.Price,
		Description: in.Description,
	}
	added, err := l.svcCtx.Store.CreateProduct(l.ctx, arg)
	if err != nil {
		return nil, status.Errorf(codes.DeadlineExceeded, "%v", err)
	}
	// parents, err := l.svcCtx.Store.CreateProductParents(l.ctx)
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// log.Println("parents_log:", parents)

	return &product.CreateProductResponse{
		Id: added.ID,
	}, nil
}
