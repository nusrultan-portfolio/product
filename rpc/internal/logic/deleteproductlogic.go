package logic

import (
	"context"
	"database/sql"
	"fmt"

	"ecom_kg/product/rpc/internal/svc"
	"ecom_kg/product/rpc/product"

	"github.com/zeromicro/go-zero/core/logx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type DeleteProductLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewDeleteProductLogic(ctx context.Context, svcCtx *svc.ServiceContext) *DeleteProductLogic {
	return &DeleteProductLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *DeleteProductLogic) DeleteProduct(in *product.DeleteProductRequest) (*product.DeleteProductResponse, error) {
	// todo: add your logic here and delete this line

	id, err := l.svcCtx.Store.DeleteProduct(l.ctx, in.Id)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, status.Errorf(codes.NotFound, "id (%v) does not exist", in.Id)
		}
		return nil, status.Errorf(codes.DeadlineExceeded, "%v", err)
	}


	return &product.DeleteProductResponse{
		Message: fmt.Sprintf("id (%v) deleted successfully", id),
	}, nil
}
