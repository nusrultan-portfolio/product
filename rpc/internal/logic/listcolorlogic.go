package logic

import (
	"context"

	model "ecom_kg/product/model/sqlc"
	"ecom_kg/product/rpc/internal/svc"
	"ecom_kg/product/rpc/product"

	"github.com/zeromicro/go-zero/core/logx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ListColorLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewListColorLogic(ctx context.Context, svcCtx *svc.ServiceContext) *ListColorLogic {
	return &ListColorLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *ListColorLogic) ListColor(in *product.ListColorsReq) (*product.ListColorsResp, error) {
	// todo: add your logic here and delete this line
	arg := model.ListColorsParams{
		Limit:  in.PageSize,
		Offset: (in.PageId - 1) * in.PageSize,
	}

	colors, err := l.svcCtx.Store.ListColors(l.ctx, arg)
	if err != nil {
		return nil, status.Errorf(codes.DeadlineExceeded, "%v", err)
	}

	var listColors = make([]product.Colors, len(colors))

	for i := 0; i < len(colors); i++ {
		listColors[i].Id = colors[i].ID
		listColors[i].Abbr = colors[i].Abbr
		listColors[i].Name = colors[i].Name.String
		listColors[i].Family = colors[i].Family.String
		listColors[i].Code = colors[i].Code.String
		listColors[i].Hex = colors[i].Hex.String
		listColors[i].CreatedAt = colors[i].CreatedAt.Time.String()
		listColors[i].UpdatedAt = colors[i].UpdatedAt.Time.String()
	}

	respColors := make([]*product.Colors, 0)
	for i := 0; i < len(listColors); i++ {
		respColors = append(respColors, &listColors[i])
	}

	return &product.ListColorsResp{
		Colors: respColors,
	}, nil
}
