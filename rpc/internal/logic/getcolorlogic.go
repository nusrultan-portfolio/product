package logic

import (
	"context"
	"database/sql"

	"ecom_kg/product/rpc/internal/svc"
	"ecom_kg/product/rpc/product"

	"github.com/zeromicro/go-zero/core/logx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type GetColorLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetColorLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetColorLogic {
	return &GetColorLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *GetColorLogic) GetColor(in *product.GetColorsReq) (*product.GetColorsResp, error) {
	// todo: add your logic here and delete this line
	get, err := l.svcCtx.Store.GetColorByID(l.ctx, in.Id)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, status.Errorf(codes.NotFound, "id (%v) not found", in.Id)
		}
		return nil, status.Errorf(codes.DeadlineExceeded, "%v", err)
	}
	return &product.GetColorsResp{
		Id:        get.ID,
		Abbr:      get.Abbr,
		Name:      get.Name.String,
		Family:    get.Family.String,
		Code:      get.Code.String,
		Hex:       get.Hex.String,
		CreatedAt: get.CreatedAt.Time.String(),
		UpdatedAt: get.UpdatedAt.Time.String(),
	}, nil
}
