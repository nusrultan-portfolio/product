package svc

import (
	"bytes"
	"context"
	"database/sql"
	model "ecom_kg/product/model/sqlc"
	"ecom_kg/product/rpc/internal/config"
	"encoding/json"
	"io"
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/elastic/go-elasticsearch/v8"
	"github.com/elastic/go-elasticsearch/v8/esapi"

	"github.com/lib/pq"
)

type ServiceContext struct {
	Config   config.Config
	Store    model.Store
	ESClient *elasticsearch.Client
}

func NewServiceContext(c config.Config) *ServiceContext {

	env, err := config.LoadConfig("./")
	if err != nil {
		log.Fatal("loading config err: ", err)
	}

	es, err := elasticsearch.NewDefaultClient()
	if err != nil {
		log.Fatal("cannot connect to ES", err)
	}

	conn, err := sql.Open("postgres", env.DataSourceName)
	if err != nil {
		log.Fatal("cannot to connect Product db:", err)
	}

	reportProblems := func(ev pq.ListenerEventType, err error) {
		if err != nil {
			log.Println(ev)
			log.Println(err.Error())
		}
	}

	listener := pq.NewListener(env.DataSourceName, 10*time.Second, time.Minute, reportProblems)
	err = listener.Listen("events")
	if err != nil {
		panic(err)
	}
	log.Printf("listening Pg events...")
	go func() {
		getNotification(listener)
	}()

	return &ServiceContext{
		Config:   c,
		Store:    model.NewStore(conn),
		ESClient: es,
	}
}

type Message struct {
	Table  string           `json:"table"`
	ID     int              `json:"id"`
	Action string           `json:"action"`
	Data   *json.RawMessage `json:"data"`
}

func getNotification(listener *pq.Listener) {
	for {
		select {
		case n := <-listener.Notify:
			var prettyJson bytes.Buffer
			err := json.Indent(&prettyJson, []byte(n.Extra), "", "\t")
			if err != nil {
				log.Println("notify json err: ", err)
				return
			}
			log.Println(prettyJson.String())

			var message Message
			bytes := prettyJson.Bytes()

			err2 := json.Unmarshal(bytes, &message)
			if err2 != nil {
				log.Println("unmarshal pretty to struct err: ", err2)
				return
			}

			writeChangesToEs(message)

		case <-time.After(90 * time.Second):
			log.Printf("nothing happened in 90 seconds..")

			go func() {
				listener.Ping()
			}()

		}
	}
}

func writeChangesToEs(message Message) {
	s := []string{message.Table, strconv.Itoa(message.ID)}
	tableAndId := strings.Join(s, "_")

	switch message.Action {
	case "DELETE":
		if !elasticReq("DELETE", tableAndId, nil) {
			log.Println("Failed to delete", tableAndId)
		}
	case "INSERT":
		r := bytes.NewReader([]byte(*message.Data))
		if !elasticReq("INSERT", tableAndId, r) {
			log.Printf("Failed to index %s: \n%s", tableAndId, string(*message.Data))
		}
	case "PUT":
		r := bytes.NewReader([]byte(*message.Data))
		if !elasticReq("PUT", tableAndId, r) {
			log.Printf("Failed to put %s: \n%s", tableAndId, string(*message.Data))
		}
	}
}

func elasticReq(method, id string, reader io.Reader) bool {
	ctx := context.Background()

	es, err := elasticsearch.NewDefaultClient()
	if err != nil {
		log.Fatal("cannot initialize", err)
	}

	switch method {
	case "DELETE":

		req := esapi.DeleteRequest{
			Index:      "product",
			DocumentID: id,
		}

		delete, err := req.Do(ctx, es)
		if err != nil {
			log.Println("failed to delete es err:", err)
			return false
		}
		log.Println("delete body", delete.Body)
		defer delete.Body.Close()

	case "INSERT":
		req := esapi.CreateRequest{
			Index:      "product",
			DocumentID: id,
			Body:       reader,
		}

		insert, err := req.Do(ctx, es)
		if err != nil {
			log.Println("failed to insert es er:", err)
			return false
		}
		log.Println("insert body", insert.Body)

		defer insert.Body.Close()

	case "PUT":
		req := esapi.UpdateRequest{
			Index:      "product",
			DocumentID: id,
			Body:       reader,
		}

		update, err := req.Do(ctx, es)
		if err != nil {
			log.Println("failed to update es er:", err)
			return false
		}
		log.Println("update body", update.Body)

		defer update.Body.Close()
	}

	return true
}
