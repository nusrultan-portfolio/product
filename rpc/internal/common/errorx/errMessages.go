package errorx

// type ErrMsg string

const (
	SpecialCharacters = "msg_spec_char"
	AlphaAndNumeric   = "msg_alpha_num"
	Alpha             = "msg_alpha"
	Numeric           = "msg_num"
	MinSize           = "msg_minimum_size"
	Required          = "msg_required"
	ValidEmail        = "msg_valid_email"
	IsBool            = "msg_not_bool"
)
