package errorx

const defaultCode = 1001

type ErrorResponse struct {
	Code int64  `json:"code,omitempty"`
	Msg  string `json:"msg,omitempty"`
	Data []Data `json:"data,omitempty"`
}

type Data struct {
	Field  string   `json:"field,omitempty"`
	ErrMsg []string `json:"errMsg,omitempty"`
}

type CodeError struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
}

type CodeErrorResponse struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
}

func NewCodeError(code int, msg string) error {
	return &CodeError{
		Code: code,
		Msg:  msg,
	}
}

func NewDefaultError(msg string) error {
	return NewCodeError(defaultCode, msg)
}

func (e *CodeError) Error() string {
	return e.Msg
}

func (e *CodeError) Data() *CodeErrorResponse {
	return &CodeErrorResponse{
		Code: e.Code,
		Msg:  e.Msg,
	}
}
