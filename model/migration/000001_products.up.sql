-- Products_table
CREATE TABLE "products" (
  "id" bigserial PRIMARY KEY,
  "title" varchar NOT NULL,
  "active" boolean NOT NULL,
  "sku" varchar NOT NULL,
  "price" float NOT NULL,
  "description" varchar NOT NULL,
  "created_at" TIMESTAMPTZ NOT NULL DEFAULT (now()),
  "updated_at" TIMESTAMPTZ NOT NULL DEFAULT (now())
);

CREATE TABLE IF NOT EXISTS "product_logs" (
    "id" SERIAL PRIMARY KEY,
    "product_id" INT NOT NULL,
    "operation" VARCHAR(20) NOT NULL,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE OR REPLACE FUNCTION public.notify_event()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

    DECLARE
        data json;
        notification json;
        id integer;
    BEGIN

        -- Convert the old or new row to JSON, based on the kind of action.
        -- Action = DELETE?             -> OLD row
        -- Action = INSERT or UPDATE?   -> NEW row
        IF (TG_OP = 'DELETE') THEN
            data = row_to_json(OLD);
            id = OLD.id;
        ELSE
            data = row_to_json(NEW);
            id = NEW.id;
        END IF;

        -- Contruct the notification as a JSON string.
        notification = json_build_object(
                          'table',TG_TABLE_NAME,
                          'action', TG_OP,
                          'id', id,
                          'data', data);

        -- Execute pg_notify(channel, notification)
        PERFORM pg_notify('events',notification::text);

        -- Result is ignored since this is an AFTER trigger
        RETURN NULL;
    END;
$BODY$;

CREATE TRIGGER products_notify_event
    AFTER INSERT OR DELETE OR UPDATE
    ON public.products
    FOR EACH ROW
    EXECUTE PROCEDURE public.notify_event();

-- Categories_table
CREATE TABLE "categories" (
  "id" bigserial PRIMARY KEY,
  "name" varchar NOT NULL,
  "slug" varchar NOT NULL,
  "created_at" TIMESTAMPTZ NOT NULL DEFAULT (now()),
  "updated_at" TIMESTAMPTZ NOT NULL DEFAULT (now())
);

-- Classes_table
CREATE TABLE "classes" (
  "id" bigserial PRIMARY KEY,
  "name" varchar NOT NULL,
  "slug" varchar NOT NULL,
  "created_at" timestamptz NULL DEFAULT NULL,
  "updated_at" timestamptz NULL DEFAULT NULL
);

-- Divisions_table
CREATE TABLE "divisions" (
  "id" bigserial PRIMARY KEY,
  "name" varchar NOT NULL,
  "slug" varchar NOT NULL,
  "created_at" timestamptz NULL DEFAULT NULL,
  "updated_at" timestamptz NULL DEFAULT NULL
);

-- Colors_table
CREATE TABLE "colors" (
  "id" bigserial PRIMARY KEY,
  "abbr" varchar NOT NULL,
  "name" varchar DEFAULT NULL,
  "family" varchar DEFAULT NULL,
  "code" varchar DEFAULT NULL,
  "hex" varchar DEFAULT NULL,
  "created_at" timestamptz NULL DEFAULT NULL,
  "updated_at" timestamptz NULL DEFAULT NULL
);

-- Groups_table
CREATE TABLE "groups" (
  "id" bigserial PRIMARY KEY,
  "name" varchar NOT NULL,
  "slug" varchar NOT NULL,
  "created_at" timestamptz NULL DEFAULT NULL,
  "updated_at" timestamptz NULL DEFAULT NULL
);

-- Product_images_table
CREATE TABLE product_images (
  id bigserial PRIMARY KEY,
  lg varchar NOT NULL,
  md varchar NOT NULL,
  sm varchar NOT NULL,
  thumb varchar NOT NULL,
  product_id bigint NOT NULL,
  created_at timestamptz NULL DEFAULT NULL,
  updated_at timestamptz NULL DEFAULT NULL
);

-- Subclasses_table
CREATE TABLE "subclasses" (
  "id" bigserial PRIMARY KEY,
  "name" varchar NOT NULL,
  "slug" varchar NOT NULL,
  "created_at" timestamptz NULL DEFAULT NULL,
  "updated_at" timestamptz NULL DEFAULT NULL
);

-- Product_parents_table
CREATE TABLE "product_parents" (
  "id" bigserial PRIMARY KEY,
  "title" varchar NOT NULL,
  "live" int NULL DEFAULT 0,--not null
  "brand_id" bigint NULL,--not null
  "sku" varchar NULL,--not null
  "desc_full" text DEFAULT NULL,
  "desc_left" text DEFAULT NULL,
  "desc_right" text DEFAULT NULL,
  "division_id" bigint NULL, --not null
  "group_id" bigint NULL,--not null
  "category_id" bigint NULL,--not null
  "class_id" bigint NULL,--not null
  "subclass_id" bigint NULL,--not null
  "featured" int NULL DEFAULT 0,--not null
  "asin" varchar DEFAULT NULL,
  "ean" varchar DEFAULT NULL,
  "e_barcode" varchar DEFAULT NULL,
  "r_barcode" varchar DEFAULT NULL,
  "non_draft" int NULL DEFAULT 0,--not null
  "draft_id" bigint DEFAULT NULL,
  "variation_id" bigint NULL DEFAULT 1,--not null
  "created_at" timestamp NULL DEFAULT NULL,
  "updated_at" timestamp NULL DEFAULT NULL,
  FOREIGN KEY (division_id) REFERENCES divisions(id),
  FOREIGN KEY (group_id) REFERENCES groups(id),
  FOREIGN KEY (category_id) REFERENCES categories(id),
  FOREIGN KEY (class_id) REFERENCES classes(id),
  FOREIGN KEY (subclass_id) REFERENCES subclasses(id)
);
-- delete draft_id

-- Sizes_table
CREATE TABLE "sizes" (
  "id" bigserial NOT NULL,
  "abbr" varchar NOT NULL,
  "name" varchar NOT NULL,
  "sizing_type_id" bigint NOT NULL,
  "created_at" timestamp NULL DEFAULT NULL,
  "updated_at" timestamp NULL DEFAULT NULL
);
