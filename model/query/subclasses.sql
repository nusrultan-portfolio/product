-- name: CreateSubclass :one
INSERT INTO "subclasses" (
    name,
    slug
) VALUES (
  $1, $2
)
RETURNING *;

-- name: GetSubclassyID :one
SELECT * FROM subclasses
WHERE id = $1 LIMIT 1;

-- name: GetSubclassByName :one
SELECT * FROM subclasses
WHERE name = $1;

-- name: ImportSubclasses :exec
INSERT INTO subclasses(
    id,name,slug
) VALUES(
    $1,$2,$3
);

-- name: ListSubclassesAll :many
SELECT * FROM subclasses
ORDER BY id;

-- name: ListSubclasses :many
SELECT * FROM subclasses
ORDER BY id
LIMIT $1
OFFSET $2;

-- name: UpdateSubclass :one
UPDATE subclasses
SET
    name = $2,
    slug = $3,
    updated_at = $4
WHERE id = $1
RETURNING *;

-- name: DeleteSubclass :one
DELETE FROM subclasses
WHERE id = $1
RETURNING id;