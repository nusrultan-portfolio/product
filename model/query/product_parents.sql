-- name: CreateProductParents :one
INSERT INTO product_parents (
    title,
    live,
    brand_id,
    sku,
    desc_full,
    desc_left,
    desc_right,
    division_id,
    group_id,
    category_id,
    class_id,
    subclass_id,
    featured,
    asin,
    ean,
    e_barcode,
    r_barcode,
    non_draft,
    draft_id,
    variation_id
) VALUES (
    $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20)
RETURNING *;

-- name: CreateParentsFromProduct :one
INSERT INTO product_parents (title,sku)
SELECT title,sku FROM products
RETURNING *;
-- WHERE created_at = $1;

-- name: GetProductParentsByID :one
SELECT * FROM product_parents
WHERE id = $1 LIMIT 1;

-- name: GetProductParentsByTitle :many
SELECT * FROM product_parents
WHERE title = $1;

-- name: ImportProductParents :exec
INSERT INTO product_parents(
    id,
    title,
    live,
    brand_id,
    sku,
    desc_full,
    desc_left,
    desc_right,
    division_id,
    group_id,
    category_id,
    class_id,
    subclass_id,
    featured,
    asin,
    ean,
    e_barcode,
    r_barcode,
    non_draft,
    draft_id,
    variation_id
) VALUES(
     $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21
);

-- name: ListProductParentsAll :many
SELECT * FROM product_parents
ORDER BY id 
LIMIT $1
OFFSET $2;

UPDATE product_parents
SET
    title = $2,
    live = $3,
    brand_id = $4,
    sku = $5,
    desc_full = $6,
    desc_left = $7,
    desc_right = $8,
    division_id = $9,
    group_id = $10,
    category_id = $11,
    class_id = $12,
    subclass_id = $13,
    featured = $14,
    asin = $15,
    ean = $16,
    e_barcode = $17,
    r_barcode = $18,
    non_draft = $19,
    draft_id = $20,
    variation_id = $21,
    updated_at = $22
WHERE id = $1
RETURNING *;

-- name: DeleteProductParents :one
DELETE FROM product_parents
WHERE id = $1
RETURNING id;