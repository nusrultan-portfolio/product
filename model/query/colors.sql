-- name: CreateColor :one
INSERT INTO "colors" (
  abbr,
  name,
  family,
  code,
  hex
) VALUES (
  $1, $2, $3, $4, $5
)
RETURNING *;

-- name: GetColorByID :one
SELECT * FROM colors
WHERE id = $1 LIMIT 1;

-- name: GetColorByName :many
SELECT * FROM colors
WHERE name = $1;

-- name: ImportColors :exec
INSERT INTO colors(
    id,
    abbr,
    name,
    family,
    code,
    hex
) VALUES(
    $1,$2,$3,$4,$5,$6
);

-- name: ListColorsAll :many
SELECT * FROM colors
ORDER BY id;

-- name: ListColors :many
SELECT * FROM colors
ORDER BY id
LIMIT $1
OFFSET $2;

-- name: UpdateColor :one
UPDATE colors
SET
  abbr = $2,
  name = $3,
  family = $4,
  code = $5,
  hex = $6,
  updated_at = $7
WHERE id = $1
RETURNING *;

-- name: DeleteColor :one
DELETE FROM colors
WHERE id = $1
RETURNING id;