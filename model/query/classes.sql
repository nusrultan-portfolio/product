-- name: CreateClass :one
INSERT INTO "classes" (
    name,
    slug
) VALUES (
  $1, $2
)
RETURNING *;

-- name: GetClassByID :one
SELECT * FROM classes
WHERE id = $1 LIMIT 1;

-- name: GetClassByName :one
SELECT * FROM classes
WHERE name = $1;

-- name: ImportClasses :exec
INSERT INTO classes(
    id,name,slug
) VALUES(
    $1,$2,$3
);

-- name: ListClassesAll :many
SELECT * FROM classes
ORDER BY id;

-- name: ListClasses :many
SELECT * FROM classes
ORDER BY id
LIMIT $1
OFFSET $2;

-- name: UpdateClass :one
UPDATE classes
SET
    name = $2,
    slug = $3,
    updated_at = $4
WHERE id = $1
RETURNING *;

-- name: DeleteClass :one
DELETE FROM classes
WHERE id = $1
RETURNING id;

