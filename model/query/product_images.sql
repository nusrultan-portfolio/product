-- name: CreateImage :one
INSERT INTO "product_images" (
    lg,
    md,
    sm,
    thumb,
    product_id
) VALUES (
  $1, $2, $3, $4, $5
)
RETURNING *;

-- name: GetImageByID :one
SELECT * FROM product_images
WHERE id = $1 LIMIT 1;

-- name: GetImageByProductID :one
SELECT * FROM product_images
WHERE product_id = $1;

-- name: ImportProductImages :exec
INSERT INTO product_images(
    id,
    lg,
    md,
    sm,
    thumb,
    product_id
) VALUES(
    $1,$2,$3,$4,$5,$6
);

-- name: ListImagesAll :many
SELECT * FROM product_images
ORDER BY id;

-- name: ListProductImages :many
SELECT * FROM product_images
ORDER BY id
LIMIT $1
OFFSET $2;

-- name: UpdateProductImage :one
UPDATE product_images
SET
    lg = $2,
    md = $3,
    sm = $4,
    thumb =$5,
    product_id =$6,
    updated_at =$5
WHERE id = $1
RETURNING *;

-- name: DeleteProductImage :one
DELETE FROM product_images
WHERE id = $1
RETURNING id;