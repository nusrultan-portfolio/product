-- name: CreateSize :one
INSERT INTO "sizes" (
    abbr,
    name,
    sizing_type_id
) VALUES (
  $1, $2, $3
)
RETURNING *;

-- name: GetSizeyID :one
SELECT * FROM sizes
WHERE id = $1 LIMIT 1;

-- name: GetSizeByName :one
SELECT * FROM sizes
WHERE name = $1;

-- name: ImportSizes :exec
INSERT INTO sizes(
    id,abbr,name,sizing_type_id
) VALUES(
    $1,$2,$3,$4
);

-- name: ListSizesAll :many
SELECT * FROM sizes
ORDER BY id;

-- name: ListSizes :many
SELECT * FROM sizes
ORDER BY id
LIMIT $1
OFFSET $2;

-- name: UpdateSize :one
UPDATE sizes
SET
    abbr = $2,
    name = $3,
    sizing_type_id = $4,
    updated_at =$5
WHERE id = $1
RETURNING *;

-- name: DeleteSize :one
DELETE FROM sizes
WHERE id = $1
RETURNING id;