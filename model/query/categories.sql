-- name: CreateCategory :one
INSERT INTO "categories" (
    name,
    slug
) VALUES (
  $1, $2
)
RETURNING *;

-- name: GetCategoryByID :one
SELECT * FROM categories
WHERE id = $1 LIMIT 1;

-- name: GetCategoryByName :one
SELECT * FROM categories
WHERE name = $1;

-- name: ImportCategories :exec
INSERT INTO categories(
    id,name,slug
) VALUES(
    $1,$2,$3
);

-- name: ListCategoriesAll :many
SELECT * FROM categories
ORDER BY id;

-- name: ListCategories :many
SELECT * FROM categories
ORDER BY id
LIMIT $1
OFFSET $2;

-- name: UpdateCategory :one
UPDATE categories
SET
  name = $2,
  slug = $3,
	updated_at = $4
WHERE id = $1
RETURNING *;

-- name: DeleteCategory :one
DELETE FROM categories
WHERE id = $1
RETURNING id;

-- name: DeleteAllCategories :exec
DELETE FROM categories;