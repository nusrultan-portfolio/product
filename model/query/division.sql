-- name: CreateDivision :one
INSERT INTO "divisions" (
    name,
    slug
) VALUES (
  $1, $2
)
RETURNING *;

-- name: GetDivisionyID :one
SELECT * FROM divisions
WHERE id = $1 LIMIT 1;

-- name: GetDivisionByName :one
SELECT * FROM divisions
WHERE name = $1;

-- name: ImportDivisions :exec
INSERT INTO divisions(
    id,name,slug
) VALUES(
    $1,$2,$3
);

-- name: ListdivisionsAll :many
SELECT * FROM divisions
ORDER BY id;

-- name: Listdivisions :many
SELECT * FROM divisions
ORDER BY id
LIMIT $1
OFFSET $2;

-- name: UpdateDivision :one
UPDATE divisions
SET
    name = $2,
    slug = $3,
    updated_at = $4
WHERE id = $1
RETURNING *;

-- name: DeleteDivision :one
DELETE FROM divisions
WHERE id = $1
RETURNING id;