-- name: CreateGroup :one
INSERT INTO "groups" (
    name,
    slug
) VALUES (
  $1, $2
)
RETURNING *;

-- name: GetGroupyID :one
SELECT * FROM groups
WHERE id = $1 LIMIT 1;

-- name: GetGroupByName :one
SELECT * FROM groups
WHERE name = $1;

-- name: ImportGroups :exec
INSERT INTO groups(
    id,name,slug
) VALUES(
    $1,$2,$3
);

-- name: ListGroupsAll :many
SELECT * FROM groups
ORDER BY id;

-- name: ListGroups :many
SELECT * FROM groups
ORDER BY id
LIMIT $1
OFFSET $2;

-- name: UpdateGroup :one
UPDATE groups
SET
    name = $2,
    slug = $3,
    updated_at = $4
WHERE id = $1
RETURNING *;

-- name: DeleteGroup :one
DELETE FROM groups
WHERE id = $1
RETURNING id;