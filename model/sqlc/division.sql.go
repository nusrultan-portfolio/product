// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.15.0
// source: division.sql

package model

import (
	"context"
	"database/sql"
)

const createDivision = `-- name: CreateDivision :one
INSERT INTO "divisions" (
    name,
    slug
) VALUES (
  $1, $2
)
RETURNING id, name, slug, created_at, updated_at
`

type CreateDivisionParams struct {
	Name string `json:"name"`
	Slug string `json:"slug"`
}

func (q *Queries) CreateDivision(ctx context.Context, arg CreateDivisionParams) (Division, error) {
	row := q.db.QueryRowContext(ctx, createDivision, arg.Name, arg.Slug)
	var i Division
	err := row.Scan(
		&i.ID,
		&i.Name,
		&i.Slug,
		&i.CreatedAt,
		&i.UpdatedAt,
	)
	return i, err
}

const deleteDivision = `-- name: DeleteDivision :one
DELETE FROM divisions
WHERE id = $1
RETURNING id
`

func (q *Queries) DeleteDivision(ctx context.Context, id int64) (int64, error) {
	row := q.db.QueryRowContext(ctx, deleteDivision, id)
	err := row.Scan(&id)
	return id, err
}

const getDivisionByName = `-- name: GetDivisionByName :one
SELECT id, name, slug, created_at, updated_at FROM divisions
WHERE name = $1
`

func (q *Queries) GetDivisionByName(ctx context.Context, name string) (Division, error) {
	row := q.db.QueryRowContext(ctx, getDivisionByName, name)
	var i Division
	err := row.Scan(
		&i.ID,
		&i.Name,
		&i.Slug,
		&i.CreatedAt,
		&i.UpdatedAt,
	)
	return i, err
}

const getDivisionyID = `-- name: GetDivisionyID :one
SELECT id, name, slug, created_at, updated_at FROM divisions
WHERE id = $1 LIMIT 1
`

func (q *Queries) GetDivisionyID(ctx context.Context, id int64) (Division, error) {
	row := q.db.QueryRowContext(ctx, getDivisionyID, id)
	var i Division
	err := row.Scan(
		&i.ID,
		&i.Name,
		&i.Slug,
		&i.CreatedAt,
		&i.UpdatedAt,
	)
	return i, err
}

const importDivisions = `-- name: ImportDivisions :exec
INSERT INTO divisions(
    id,name,slug
) VALUES(
    $1,$2,$3
)
`

type ImportDivisionsParams struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
	Slug string `json:"slug"`
}

func (q *Queries) ImportDivisions(ctx context.Context, arg ImportDivisionsParams) error {
	_, err := q.db.ExecContext(ctx, importDivisions, arg.ID, arg.Name, arg.Slug)
	return err
}

const listdivisions = `-- name: Listdivisions :many
SELECT id, name, slug, created_at, updated_at FROM divisions
ORDER BY id
LIMIT $1
OFFSET $2
`

type ListdivisionsParams struct {
	Limit  int32 `json:"limit"`
	Offset int32 `json:"offset"`
}

func (q *Queries) Listdivisions(ctx context.Context, arg ListdivisionsParams) ([]Division, error) {
	rows, err := q.db.QueryContext(ctx, listdivisions, arg.Limit, arg.Offset)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var items []Division
	for rows.Next() {
		var i Division
		if err := rows.Scan(
			&i.ID,
			&i.Name,
			&i.Slug,
			&i.CreatedAt,
			&i.UpdatedAt,
		); err != nil {
			return nil, err
		}
		items = append(items, i)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}

const listdivisionsAll = `-- name: ListdivisionsAll :many
SELECT id, name, slug, created_at, updated_at FROM divisions
ORDER BY id
`

func (q *Queries) ListdivisionsAll(ctx context.Context) ([]Division, error) {
	rows, err := q.db.QueryContext(ctx, listdivisionsAll)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var items []Division
	for rows.Next() {
		var i Division
		if err := rows.Scan(
			&i.ID,
			&i.Name,
			&i.Slug,
			&i.CreatedAt,
			&i.UpdatedAt,
		); err != nil {
			return nil, err
		}
		items = append(items, i)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}

const updateDivision = `-- name: UpdateDivision :one
UPDATE divisions
SET
    name = $2,
    slug = $3,
    updated_at = $4
WHERE id = $1
RETURNING id, name, slug, created_at, updated_at
`

type UpdateDivisionParams struct {
	ID        int64        `json:"id"`
	Name      string       `json:"name"`
	Slug      string       `json:"slug"`
	UpdatedAt sql.NullTime `json:"updated_at"`
}

func (q *Queries) UpdateDivision(ctx context.Context, arg UpdateDivisionParams) (Division, error) {
	row := q.db.QueryRowContext(ctx, updateDivision,
		arg.ID,
		arg.Name,
		arg.Slug,
		arg.UpdatedAt,
	)
	var i Division
	err := row.Scan(
		&i.ID,
		&i.Name,
		&i.Slug,
		&i.CreatedAt,
		&i.UpdatedAt,
	)
	return i, err
}
