// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.15.0
// source: categories.sql

package model

import (
	"context"
	"time"
)

const createCategory = `-- name: CreateCategory :one
INSERT INTO "categories" (
    name,
    slug
) VALUES (
  $1, $2
)
RETURNING id, name, slug, created_at, updated_at
`

type CreateCategoryParams struct {
	Name string `json:"name"`
	Slug string `json:"slug"`
}

func (q *Queries) CreateCategory(ctx context.Context, arg CreateCategoryParams) (Category, error) {
	row := q.db.QueryRowContext(ctx, createCategory, arg.Name, arg.Slug)
	var i Category
	err := row.Scan(
		&i.ID,
		&i.Name,
		&i.Slug,
		&i.CreatedAt,
		&i.UpdatedAt,
	)
	return i, err
}

const deleteAllCategories = `-- name: DeleteAllCategories :exec
DELETE FROM categories
`

func (q *Queries) DeleteAllCategories(ctx context.Context) error {
	_, err := q.db.ExecContext(ctx, deleteAllCategories)
	return err
}

const deleteCategory = `-- name: DeleteCategory :one
DELETE FROM categories
WHERE id = $1
RETURNING id
`

func (q *Queries) DeleteCategory(ctx context.Context, id int64) (int64, error) {
	row := q.db.QueryRowContext(ctx, deleteCategory, id)
	err := row.Scan(&id)
	return id, err
}

const getCategoryByID = `-- name: GetCategoryByID :one
SELECT id, name, slug, created_at, updated_at FROM categories
WHERE id = $1 LIMIT 1
`

func (q *Queries) GetCategoryByID(ctx context.Context, id int64) (Category, error) {
	row := q.db.QueryRowContext(ctx, getCategoryByID, id)
	var i Category
	err := row.Scan(
		&i.ID,
		&i.Name,
		&i.Slug,
		&i.CreatedAt,
		&i.UpdatedAt,
	)
	return i, err
}

const getCategoryByName = `-- name: GetCategoryByName :one
SELECT id, name, slug, created_at, updated_at FROM categories
WHERE name = $1
`

func (q *Queries) GetCategoryByName(ctx context.Context, name string) (Category, error) {
	row := q.db.QueryRowContext(ctx, getCategoryByName, name)
	var i Category
	err := row.Scan(
		&i.ID,
		&i.Name,
		&i.Slug,
		&i.CreatedAt,
		&i.UpdatedAt,
	)
	return i, err
}

const importCategories = `-- name: ImportCategories :exec
INSERT INTO categories(
    id,name,slug
) VALUES(
    $1,$2,$3
)
`

type ImportCategoriesParams struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
	Slug string `json:"slug"`
}

func (q *Queries) ImportCategories(ctx context.Context, arg ImportCategoriesParams) error {
	_, err := q.db.ExecContext(ctx, importCategories, arg.ID, arg.Name, arg.Slug)
	return err
}

const listCategories = `-- name: ListCategories :many
SELECT id, name, slug, created_at, updated_at FROM categories
ORDER BY id
LIMIT $1
OFFSET $2
`

type ListCategoriesParams struct {
	Limit  int32 `json:"limit"`
	Offset int32 `json:"offset"`
}

func (q *Queries) ListCategories(ctx context.Context, arg ListCategoriesParams) ([]Category, error) {
	rows, err := q.db.QueryContext(ctx, listCategories, arg.Limit, arg.Offset)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var items []Category
	for rows.Next() {
		var i Category
		if err := rows.Scan(
			&i.ID,
			&i.Name,
			&i.Slug,
			&i.CreatedAt,
			&i.UpdatedAt,
		); err != nil {
			return nil, err
		}
		items = append(items, i)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}

const listCategoriesAll = `-- name: ListCategoriesAll :many
SELECT id, name, slug, created_at, updated_at FROM categories
ORDER BY id
`

func (q *Queries) ListCategoriesAll(ctx context.Context) ([]Category, error) {
	rows, err := q.db.QueryContext(ctx, listCategoriesAll)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var items []Category
	for rows.Next() {
		var i Category
		if err := rows.Scan(
			&i.ID,
			&i.Name,
			&i.Slug,
			&i.CreatedAt,
			&i.UpdatedAt,
		); err != nil {
			return nil, err
		}
		items = append(items, i)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}

const updateCategory = `-- name: UpdateCategory :one
UPDATE categories
SET
  name = $2,
  slug = $3,
	updated_at = $4
WHERE id = $1
RETURNING id, name, slug, created_at, updated_at
`

type UpdateCategoryParams struct {
	ID        int64     `json:"id"`
	Name      string    `json:"name"`
	Slug      string    `json:"slug"`
	UpdatedAt time.Time `json:"updated_at"`
}

func (q *Queries) UpdateCategory(ctx context.Context, arg UpdateCategoryParams) (Category, error) {
	row := q.db.QueryRowContext(ctx, updateCategory,
		arg.ID,
		arg.Name,
		arg.Slug,
		arg.UpdatedAt,
	)
	var i Category
	err := row.Scan(
		&i.ID,
		&i.Name,
		&i.Slug,
		&i.CreatedAt,
		&i.UpdatedAt,
	)
	return i, err
}
