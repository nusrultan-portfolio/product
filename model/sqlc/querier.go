package model

import (
	"context"
	"database/sql"
)

type Querier interface {
	CreateProduct(ctx context.Context, arg CreateProductParams) (Product, error)
	GetProduct(ctx context.Context, id int64) (Product, error)
	ImportProducts(ctx context.Context, arg ImportProductsParams) error
	ListProducts(ctx context.Context, arg ListProductsParams) ([]Product, error)
	UpdateProduct(ctx context.Context, arg UpdateProductParams) (Product, error)
	DeleteProduct(ctx context.Context, id int64) (int64, error)

	CreateCategory(ctx context.Context, arg CreateCategoryParams) (Category, error)
	DeleteAllCategories(ctx context.Context) error
	DeleteCategory(ctx context.Context, id int64) (int64, error)
	GetCategoryByID(ctx context.Context, id int64) (Category, error)
	GetCategoryByName(ctx context.Context, name string) (Category, error)
	ImportCategories(ctx context.Context, arg ImportCategoriesParams) error
	ListCategories(ctx context.Context, arg ListCategoriesParams) ([]Category, error)
	ListCategoriesAll(ctx context.Context) ([]Category, error)
	UpdateCategory(ctx context.Context, arg UpdateCategoryParams) (Category, error)

	CreateColor(ctx context.Context, arg CreateColorParams) (Color, error)
	DeleteColor(ctx context.Context, id int64) (int64, error)
	GetColorByID(ctx context.Context, id int64) (Color, error)
	GetColorByName(ctx context.Context, name sql.NullString) ([]Color, error)
	ImportColors(ctx context.Context, arg ImportColorsParams) error
	ListColors(ctx context.Context, arg ListColorsParams) ([]Color, error)
	ListColorsAll(ctx context.Context) ([]Color, error)
	UpdateColor(ctx context.Context, arg UpdateColorParams) (Color, error)

	CreateClass(ctx context.Context, arg CreateClassParams) (Class, error)
	DeleteClass(ctx context.Context, id int64) (int64, error)
	GetClassByID(ctx context.Context, id int64) (Class, error)
	GetClassByName(ctx context.Context, name string) (Class, error)
	ImportClasses(ctx context.Context, arg ImportClassesParams) error
	ListClasses(ctx context.Context, arg ListClassesParams) ([]Class, error)
	ListClassesAll(ctx context.Context) ([]Class, error)
	UpdateClass(ctx context.Context, arg UpdateClassParams) (Class, error)

	CreateDivision(ctx context.Context, arg CreateDivisionParams) (Division, error)
	DeleteDivision(ctx context.Context, id int64) (int64, error)
	GetDivisionByName(ctx context.Context, name string) (Division, error)
	GetDivisionyID(ctx context.Context, id int64) (Division, error)
	ImportDivisions(ctx context.Context, arg ImportDivisionsParams) error
	Listdivisions(ctx context.Context, arg ListdivisionsParams) ([]Division, error)
	ListdivisionsAll(ctx context.Context) ([]Division, error)
	UpdateDivision(ctx context.Context, arg UpdateDivisionParams) (Division, error)

	CreateGroup(ctx context.Context, arg CreateGroupParams) (Group, error)
	DeleteGroup(ctx context.Context, id int64) (int64, error)
	GetGroupByName(ctx context.Context, name string) (Group, error)
	GetGroupyID(ctx context.Context, id int64) (Group, error)
	ImportGroups(ctx context.Context, arg ImportGroupsParams) error
	ListGroups(ctx context.Context, arg ListGroupsParams) ([]Group, error)
	ListGroupsAll(ctx context.Context) ([]Group, error)
	UpdateGroup(ctx context.Context, arg UpdateGroupParams) (Group, error)

	CreateImage(ctx context.Context, arg CreateImageParams) (ProductImage, error)
	DeleteProductImage(ctx context.Context, id int64) (int64, error)
	GetImageByID(ctx context.Context, id int64) (ProductImage, error)
	GetImageByProductID(ctx context.Context, productID int64) (ProductImage, error)
	ImportProductImages(ctx context.Context, arg ImportProductImagesParams) error
	ListImagesAll(ctx context.Context) ([]ProductImage, error)
	ListProductImages(ctx context.Context, arg ListProductImagesParams) ([]ProductImage, error)
	UpdateProductImage(ctx context.Context, arg UpdateProductImageParams) (ProductImage, error)

	CreateSize(ctx context.Context, arg CreateSizeParams) (Size, error)
	DeleteSize(ctx context.Context, id int64) (int64, error)
	GetSizeByName(ctx context.Context, name string) (Size, error)
	GetSizeyID(ctx context.Context, id int64) (Size, error)
	ImportSizes(ctx context.Context, arg ImportSizesParams) error
	ListSizes(ctx context.Context, arg ListSizesParams) ([]Size, error)
	ListSizesAll(ctx context.Context) ([]Size, error)
	UpdateSize(ctx context.Context, arg UpdateSizeParams) (Size, error)

	CreateSubclass(ctx context.Context, arg CreateSubclassParams) (Subclass, error)
	DeleteSubclass(ctx context.Context, id int64) (int64, error)
	GetSubclassByName(ctx context.Context, name string) (Subclass, error)
	GetSubclassyID(ctx context.Context, id int64) (Subclass, error)
	ImportSubclasses(ctx context.Context, arg ImportSubclassesParams) error
	ListSubclasses(ctx context.Context, arg ListSubclassesParams) ([]Subclass, error)
	ListSubclassesAll(ctx context.Context) ([]Subclass, error)
	UpdateSubclass(ctx context.Context, arg UpdateSubclassParams) (Subclass, error)

	CreateParentsFromProduct(ctx context.Context) (ProductParent, error)
	CreateProductParents(ctx context.Context, arg CreateProductParentsParams) (ProductParent, error)
	DeleteProductParents(ctx context.Context, id int64) (int64, error)
	GetProductParentsByID(ctx context.Context, id int64) (ProductParent, error)
	GetProductParentsByTitle(ctx context.Context, title string) ([]ProductParent, error)
	ImportProductParents(ctx context.Context, arg ImportProductParentsParams) error
	ListProductParentsAll(ctx context.Context, arg ListProductParentsAllParams) ([]ProductParent, error)
}

var _ Querier = (*Queries)(nil)
