.PHONY: run gen db psql createdb dropdb migrateup migratedown

gen:
	goctl rpc protoc rpc/product.proto --go_out=rpc/ --go-grpc_out=rpc/ --zrpc_out=rpc/.

run:
	go run rpc/product.go -f rpc/etc/product.yaml

db:
	docker run --name pgproduct -p 5432:5432 -e POSTGRES_USER=pgadmin -e POSTGRES_PASSWORD=Secret@123 -d postgres

psql:
	docker exec -it product-postgres-1 psql -U pgadmin product

sql:
	sqlc generate

createdb:
	docker exec -it product-postgres-1 createdb --username=pgadmin product
dropdb:
	docker exec -it product-postgres-1 dropdb --username=pgadmin product

migrateup:
	migrate -path model/migration -database "postgresql://pgadmin:Secret@123@localhost:5432/product?sslmode=disable" -verbose up

migratedown:
	migrate -path model/migration -database "postgresql://pgadmin:Secret@123@localhost:5432/product?sslmode=disable" -verbose down
